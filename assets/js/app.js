/*
 * Se esta utilizando la libreria de Phaser.
 * Inicamos creando un canvas en el cual se va usar como el entorno en donde se modelar 
 * Todo el escenario grafico.
*/

// Definicion de Variables globales, que se van a inicializar con los valores de los diferentes objetos que existe.
// Card es la variable principal, es la tarjeta que el usuario puede mover
// por todo el escenario.
var card;
var start;
var end;
var cursors;

/*
* Emieza con la declaración:
*             Variables que almacenas los indicadores por todo el juego.
*/
var video_1418;
var visto;
var video;
var video_1880;
var video_1979;

var punto_mapa;
var play_button;
var ok_verde;
var lapiz_verde;

var visto_2;
var video_2;
var punto_mapa_2;
var play_button_2;
var ok_verde_2;
var lapiz_verde_2;
var high_2;


var visto_3;
var video_3;
var punto_mapa_3;
var play_button_3;
var ok_verde_3;
var lapiz_verde_3;
var high_3;

/*
* Fin de  la declaración:
*             Variables que almacenas los indicadores por todo el juego.
*/

// Creamos el escenario o canvas de 660 de ancho, por un alto de 800
// Se esta cargando el camvas en el tag .div de index.html con id [#phaser-example]
var game = new Phaser.Game(660, 900, Phaser.CANVAS, 'phaser-example', { preload: preload, create: create, update: update, render: render });

/*
    Phaser se divide en tres Partes/Funciones importantes.
    1. Preload(): Aqui se declara todo lo relacionado con lo qe vaya usar durante la ejecución del canvas.
                    Por Ejemplo. Necesitamos cargar todas las imagnes que se van a mostrar al usaurio.
    2. create(): Esta función se encarga de crear el escenario de Phaser, enseguida crear cada uno de los 
       objetos que van a existir alli. En este caso creamos el scenario, con un fondo y los demas images con sus
       respectivas coordenas en los ejes x,y. Aclarando que es un scenario 2D.

    3. update(): Esta función es la que se encarga de toda la parte de renderizado de nuestro scenario.
                Esta se encarga de aplicar las animaciones, fisicas, etc. en tiempo de ejecución.
                El renderizado se hace con una frecuencia que calcula la libreria automaticamente
                con base la descripción de hardward, Por ejemplo. El hace el update() a una velocidad 
                de N cuadros por segundo. Este N lo define la libreria, con base a al poder 
                grafico que la computadora posee.

*/
// Precarmgamos los recursos necesarios, en este caso son imagenes.
function preload() {

    game.load.image('backdrop', './assets/imgs/bicycle.png');
    game.load.image('card', './assets/imgs/mana_card.png');

    game.load.image('diamond', './assets/imgs/diamond.png');
    game.load.image('dude', './assets/imgs/dude.png');

    ///********///////
    game.load.image('visto', './assets/imgs/visto.png');
    game.load.image('video', './assets/imgs/video.png');
    game.load.image('punto_mapa', './assets/imgs/punto_mapa.png');
    game.load.image('play_button', './assets/imgs/play-button.png');
    game.load.image('ok_verde', './assets/imgs/ok-verde.png');
    game.load.image('lapiz_verde', './assets/imgs/lapiz-verde.png');
    game.load.image('high', './assets/imgs/high.png');
    game.load.image('start', './assets/imgs/start.png');
    game.load.image('end', './assets/imgs/finished.png');

}

/*
Configuración del Canvas 2D
           Y
     _0_1_2_3_4_5_6_7_8_9_10_11_
    0|                          |
  X 1|                          |
    2|                          |
    3|                          |
    4|                          |
    5|                          |
    6|                          |
    7|                          |

*/

/*
    2. create(): Esta función se encarga de crear el escenario de Phaser, enseguida crear cada uno de los 
       objetos que van a existir alli. En este caso creamos el scenario, con un fondo y los demas images con sus
       respectivas coordenas en los ejes x,y. Aclarando que es un scenario 2D.
*/
function create() {


    //Se crea todo el escenario inicial con las fisicas y demas para la interaccion
    game.forceSingleUpdate = true;

    game.world.setBounds(0, 0, 660, 7690);

    game.add.sprite(0, 0, 'backdrop');

    card = game.add.sprite(40, 200, 'card');
    card.width = 100;card.height = 100;

    game.physics.enable(card, Phaser.Physics.ARCADE);
    card.body.collideWorldBounds = true;

    game.physics.arcade.enable(card);

    game.camera.follow(card);

    //Se hace la instancia de cursos para poder usar las flechas del teclado como entrada
    cursors = game.input.keyboard.createCursorKeys();



    //Todo lo que sigue de aca para abajo, es para crear cada uno de los iconos con los que va
    //a interactuar la bicicleta.
    
    video_1418=game.add.sprite(309, 347, 'video');
    video_1418.width = 45;video_1418.height = 45;

    //En esta seccion se le asigna fisica al objeto video_1418, y asi se hace para todos los iconos
    game.physics.arcade.enable(video_1418);


    visto = game.add.sprite(309, 552, 'visto');
    visto.width = 45; visto.height = 45;

    game.physics.arcade.enable(visto);

    video = game.add.sprite(309, 727.20, 'video');

    video.width = 45; video.height = 45;

    game.physics.arcade.enable(video);

    punto_mapa = game.add.sprite(309, 1380, 'punto_mapa');
    punto_mapa.width = 45; punto_mapa.height = 45;

    game.physics.arcade.enable(punto_mapa);

    play_button = game.add.sprite(309, 1810, 'play_button');
    play_button.width = 45; play_button.height = 45;

    game.physics.arcade.enable(play_button);

    ok_verde = game.add.sprite(303, 2465.92, 'ok_verde');
    ok_verde.width = 60; ok_verde.height = 50;

    game.physics.arcade.enable(ok_verde);

    lapiz_verde = game.add.sprite(315, 2677.68, 'lapiz_verde');
    lapiz_verde.width = 45; lapiz_verde.height = 45;

    game.physics.arcade.enable(lapiz_verde);

    video_1880 = game.add.sprite(309, 2873, 'video');
    video_1880.width = 50; video_1880.height = 50;

    game.physics.arcade.enable(video_1880);

    visto_2 = game.add.sprite(309, 3080.52, 'visto');
    visto_2.width = 45; visto_2.height = 45;

    game.physics.arcade.enable(visto_2);

    video_2 = game.add.sprite(309, 3684.32, 'video');
    video_2.width = 45; video_2.height = 45;

    game.physics.arcade.enable(video_2);

    punto_mapa_2 = game.add.sprite(309, 3871, 'punto_mapa');
    punto_mapa_2.width = 45; punto_mapa_2.height = 45;

    game.physics.arcade.enable(punto_mapa_2);

    play_button_2 = game.add.sprite(309, 4804, 'play_button');
    play_button_2.width = 45; play_button_2.height = 45;

    game.physics.arcade.enable(play_button_2);

    ok_verde_2 = game.add.sprite(309, 4995, 'ok_verde');
    ok_verde_2.width = 45; ok_verde_2.height = 45;

    game.physics.arcade.enable(ok_verde_2);

    lapiz_verde_2 = game.add.sprite(312, 5510, 'lapiz_verde');
    lapiz_verde_2.width = 45; lapiz_verde_2.height = 45;

    game.physics.arcade.enable(lapiz_verde_2);

    high_2 = game.add.sprite(309, 6365.68, 'high');
    high_2.width = 45; high_2.height = 45;

    game.physics.arcade.enable(high_2);

    ok_verde_3 = game.add.sprite(305, 5917, 'ok_verde');
    ok_verde_3.width = 60; ok_verde_3.height = 50;

    game.physics.arcade.enable(ok_verde_3);

    lapiz_verde_3 = game.add.sprite(312, 6145, 'lapiz_verde');
    lapiz_verde_3.width = 45; lapiz_verde_3.height = 45;

    game.physics.arcade.enable(lapiz_verde_3);

    high_3 = game.add.sprite(309, 6350, 'play_button');
    high_3.width = 45; high_3.height = 45;

    game.physics.arcade.enable(high_3);

    start=game.add.sprite(40, 130, 'start');
    start.width = 75;start.height = 65;

    end=game.add.sprite(300, 7505, 'end');
    end.width = 300;end.height = 150;

    //Aca terminaron de crearse los iconos y de agregarle las fisicas
    
}


/*
    3. update(): Esta función es la que se encarga de toda la parte de renderizado de nuestro scenario.
                Esta se encarga de aplicar las animaciones, fisicas, etc. en tiempo de ejecución.
                El renderizado se hace con una frecuencia que calcula la libreria automaticamente
                con base la descripción de hardward, Por ejemplo. El hace el update() a una velocidad 
                de N cuadros por segundo. Este N lo define la libreria, con base a al poder 
                grafico que la computadora posee.
*/


function update() {

    // Configuración inicial de la velocidad del objeto que el usuario va a manipular por medio del
    // las teclas de dirección.
    card.body.velocity.x = 0;
    card.body.velocity.y = 0;

    /* Si el usuario 
        preciona flecha izquierda, se aplica una velocidad
        de tipo negativa repecto al eje x
    */
    if (cursors.left.isDown) {
        // card.x -= 4;
        card.body.velocity.x = -240;
    }

    /* Si el usuario
       preciona flecha derecha, se aplica una velocidad
       de tipo positiva repecto al eje x
   */
    else if (cursors.right.isDown) {
        // card.x += 4;
        card.body.velocity.x = 240;
    }
    /* Si el usuario
           preciona flecha arriba, se aplica una velocidad
           de tipo negativa repecto al eje Y
       */
    if (cursors.up.isDown) {
        // card.y -= 4;
        card.body.velocity.y = -240;
    }
    /* Si el usuario
        preciona flecha abajo, se aplica una velocidad
        de tipo positiva repecto al eje Y
    */
    else if (cursors.down.isDown) {
        // card.y += 4;
        card.body.velocity.y = 240;
    }

    //En el update se va verifcando que el usuario a medida que va presionando las flechas del teclado,
    //Vaya revisando si paso por encima de algun icono para que muestre la informacion y posteriormente lo destruya
    //asi se hace para todos los iconos del mapa.

    //La funcion "popUp2", es llamada para crear el alert y destruir el icono.

    //Asi se hace para todos lo iconos
    game.physics.arcade.overlap(card, video_1418, popUp2, null, this);
    game.physics.arcade.overlap(card, visto, popUp1, null, this);
    game.physics.arcade.overlap(card, video, popUp3, null, this);
    game.physics.arcade.overlap(card, punto_mapa, popUp4, null, this);
    game.physics.arcade.overlap(card, play_button, popUp5, null, this);
    game.physics.arcade.overlap(card, ok_verde, popUp6, null, this);
    game.physics.arcade.overlap(card, lapiz_verde, popUp7, null, this);
    game.physics.arcade.overlap(card, video_1880, popUp8, null, this);
    game.physics.arcade.overlap(card, visto_2, popUp9, null, this);
    game.physics.arcade.overlap(card, video_2, popUp10, null, this);
    game.physics.arcade.overlap(card, punto_mapa_2, popUp11, null, this);
    game.physics.arcade.overlap(card, play_button_2, popUp12, null, this);
    game.physics.arcade.overlap(card, ok_verde_2, popUp13, null, this);
    game.physics.arcade.overlap(card, lapiz_verde_2, popUp14, null, this);
    game.physics.arcade.overlap(card, high_2, popUp15, null, this);
    game.physics.arcade.overlap(card, ok_verde_3, popUp16, null, this);
    game.physics.arcade.overlap(card, lapiz_verde_3, popUp17, null, this);
    game.physics.arcade.overlap(card, high_3, popUp18, null, this);

}


//Funcion importante, ya que genera el alert, y elimina el icono del mapa.
function popUp2(c, v) {

    //Aca es donde el usuario ve la informacion en pantalla una vez realiza la colision. 
    alert("1418, A Giovanni Fontana se le atribuye la construcción del primer vehículo"
        + " terrestre de propulsión humana: tenía cuatro ruedas y usaba una cuerda continua"
        + " conectada mediante engranajes a las ruedas. Las imágenes y una descripción más detallada no parecen existir.");
    v.destroy(); // destruir el objeto.
}

//Lo mismo se hace para cada objeto.
function popUp1(c, v) {

    alert("1790, A Comte de Sicrac se le atribuye la construcción del celerifer, supuestamente un caballo de"
        + "pasatiempo con dos ruedas en lugar de un balancin. Esto ahora se considera un engaño patriotico creado por un historiador francés en 1891."
        + "Fue desacreditado por un investigador francés en 1976. De hecho, un Jean Sievrac (!) De Marsella "
        + "obtuvo un precio de importación para un entrenador de velocidad de cuatro ruedas llamado celerifer en 1817");
    v.destroy();
}


function popUp3(c, v) {

    alert("1817, un caballo de"
        + "pasatiempo con dos ruedas en lugar de un balancin. Esto ahora se considera un engaño patriotico creado por un historiador francés en 1891."
        + "Fue desacreditado por un investigador francés en 1976. De hecho, un Jean Sievrac (!) De Marsella "
        + "obtuvo un precio de importación para un entrenador de velocidad de cuatro ruedas llamado celerifer en 1817");
    v.destroy();
}



function popUp4(c, v) {

    alert("1866, se le atribuye la construcción del celerifer, supuestamente un caballo de"
        + "pasatiempo con dos ruedas en lugar de un balancin. Esto ahora se considera un engaño patriotico creado por un historiador francés en 1891."
        + "Fue desacreditado por un investigador francés en 1976. De hecho, un Jean Sievrac (!) De Marsella "
        + "obtuvo un precio de importación para un entrenador de velocidad de cuatro ruedas llamado celerifer en 1817");
    v.destroy();
}


function popUp5(c, v) {

    alert("1870, supuestamente un caballo de"
        + "pasatiempo con dos ruedas en lugar de un balancin. Esto ahora se considera un engaño patriotico creado por un historiador francés en 1891."
        + "Fue desacreditado por un investigador francés en 1976. De hecho, un Jean Sievrac (!) De Marsella "
        + "obtuvo un precio de importación para un entrenador de velocidad de cuatro ruedas llamado celerifer en 1817");
    v.destroy();
}


function popUp6(c, v) {

    alert("1878, A Comte de Sicrac se le atribuye la construcción del celerifer, supuestamente un caballo de"
        + "pasatiempo con dos ruedas en lugar de un balancin. Esto ahora se considera un engaño patriotico creado por un historiador francés en 1891."
        + "Fue desacreditado por un investigador francés en 1976. De hecho, un Jean Sievrac (!) De Marsella "
        + "obtuvo un precio de importación para un entrenador de velocidad de cuatro ruedas llamado celerifer en 1817");
    v.destroy();
}

function popUp7(c, v) {

    alert("1879, Sicrac se le atribuye la construcción del celerifer, supuestamente un caballo de"
        + "pasatiempo con dos ruedas en lugar de un balancin. Esto ahora se considera un engaño patriotico creado por un historiador francés en 1891."
        + "Fue desacreditado por un investigador francés en 1976. De hecho, un Jean Sievrac (!) De Marsella "
        + "obtuvo un precio de importación para un entrenador de velocidad de cuatro ruedas llamado celerifer en 1817");
    v.destroy();
}


function popUp8(c, v) {

    alert("1880, atribuye la construcción del celerifer, supuestamente un caballo de"
        + "pasatiempo con dos ruedas en lugar de un balancin. Esto ahora se considera un engaño patriotico creado por un historiador francés en 1891."
        + "Fue desacreditado por un investigador francés en 1976. De hecho, un Jean Sievrac (!) De Marsella "
        + "obtuvo un precio de importación para un entrenador de velocidad de cuatro ruedas llamado celerifer en 1817");
    v.destroy();
}


function popUp9(c, v) {

    alert("1885, supuestamente un caballo de"
        + "pasatiempo con dos ruedas en lugar de un balancin. Esto ahora se considera un engaño patriotico creado por un historiador francés en 1891."
        + "Fue desacreditado por un investigador francés en 1976. De hecho, un Jean Sievrac (!) De Marsella "
        + "obtuvo un precio de importación para un entrenador de velocidad de cuatro ruedas llamado celerifer en 1817");
    v.destroy();
}


function popUp10(c, v) {

    alert("1889, la construcción del celerifer, supuestamente un caballo de"
        + "pasatiempo con dos ruedas en lugar de un balancin. Esto ahora se considera un engaño patriotico creado por un historiador francés en 1891."
        + "Fue desacreditado por un investigador francés en 1976. De hecho, un Jean Sievrac (!) De Marsella "
        + "obtuvo un precio de importación para un entrenador de velocidad de cuatro ruedas llamado celerifer en 1817");
    v.destroy();
}


function popUp11(c, v) {

    alert("1894, A Comte de Sicrac se le atribuye la construcción del celerifer, supuestamente un caballo de"
        + "pasatiempo con dos ruedas en lugar de un balancin. Esto ahora se considera un engaño patriotico creado por un historiador francés en 1891."
        + "Fue desacreditado por un investigador francés en 1976. De hecho, un Jean Sievrac (!) De Marsella "
        + "obtuvo un precio de importación para un entrenador de velocidad de cuatro ruedas llamado celerifer en 1817");
    v.destroy();
}


function popUp12(c, v) {

    alert("1924, Sicrac se le atribuye la construcción del celerifer, supuestamente un caballo de"
        + "pasatiempo con dos ruedas en lugar de un balancin. Esto ahora se considera un engaño patriotico creado por un historiador francés en 1891."
        + "Fue desacreditado por un investigador francés en 1976. De hecho, un Jean Sievrac (!) De Marsella "
        + "obtuvo un precio de importación para un entrenador de velocidad de cuatro ruedas llamado celerifer en 1817");
    v.destroy();
}

function popUp13(c, v) {

    alert("1933, Sicrac se le atribuye la construcción del celerifer, supuestamente un caballo de"
        + "pasatiempo con dos ruedas en lugar de un balancin. Esto ahora se considera un engaño patriotico creado por un historiador francés en 1891."
        + "Fue desacreditado por un investigador francés en 1976. De hecho, un Jean Sievrac (!) De Marsella "
        + "obtuvo un precio de importación para un entrenador de velocidad de cuatro ruedas llamado celerifer en 1817");
    v.destroy();
}

function popUp14(c, v) {

    alert("1979, la construcción del celerifer, supuestamente un caballo de"
        + "pasatiempo con dos ruedas en lugar de un balancin. Esto ahora se considera un engaño patriotico creado por un historiador francés en 1891."
        + "Fue desacreditado por un investigador francés en 1976. De hecho, un Jean Sievrac (!) De Marsella "
        + "obtuvo un precio de importación para un entrenador de velocidad de cuatro ruedas llamado celerifer en 1817");
    v.destroy();
}

function popUp15(c, v) {

    alert("1989, A Comte de Sicrac se le atribuye la construcción del celerifer, supuestamente un caballo de"
        + "pasatiempo con dos ruedas en lugar de un balancin. Esto ahora se considera un engaño patriotico creado por un historiador francés en 1891."
        + "Fue desacreditado por un investigador francés en 1976. De hecho, un Jean Sievrac (!) De Marsella "
        + "obtuvo un precio de importación para un entrenador de velocidad de cuatro ruedas llamado celerifer en 1817");
    v.destroy();
}

function popUp16(c, v) {

    alert("1989, supuestamente un caballo de"
        + "pasatiempo con dos ruedas en lugar de un balancin. Esto ahora se considera un engaño patriotico creado por un historiador francés en 1891."
        + "Fue desacreditado por un investigador francés en 1976. De hecho, un Jean Sievrac (!) De Marsella "
        + "obtuvo un precio de importación para un entrenador de velocidad de cuatro ruedas llamado celerifer en 1817");
    v.destroy();
}

function popUp17(c, v) {

    alert("1989, se le atribuye la construcción del celerifer, supuestamente un caballo de"
        + "pasatiempo con dos ruedas en lugar de un balancin. Esto ahora se considera un engaño patriotico creado por un historiador francés en 1891."
        + "Fue desacreditado por un investigador francés en 1976. De hecho, un Jean Sievrac (!) De Marsella "
        + "obtuvo un precio de importación para un entrenador de velocidad de cuatro ruedas llamado celerifer en 1817");
    v.destroy();
}

function popUp18(c, v) {
    
        alert("1991, la construcción del celerifer, supuestamente un caballo de"
            + "pasatiempo con dos ruedas en lugar de un balancin. Esto ahora se considera un engaño patriotico creado por un historiador francés en 1891."
            + "Fue desacreditado por un investigador francés en 1976. De hecho, un Jean Sievrac (!) De Marsella "
            + "obtuvo un precio de importación para un entrenador de velocidad de cuatro ruedas llamado celerifer en 1817");
        v.destroy();
    }

// esta funcion es epecialmente para hacer visualizar lo que sucede cada vez que se 
// interactua con el escenario.
function render() {


    //Estas lineas estan comentadas, para no crear la informacio de la camara en la pagina, sin embargo
    //Se utilizo para poder saber la uniccion en X y Y correcta para cada icono.

    // game.debug.cameraInfo(game.camera, 500, 32);
    // game.debug.spriteCoords(card, 32, 32);

    // game.debug.rectangle({ x: 400 + game.camera.x, y: 0 + game.camera.y, width: 1, height: 600 });
    // game.debug.rectangle({ x: 0 + game.camera.x, y: 300 + game.camera.y, width: 800, height: 1 });
}
